<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trace extends CI_Controller {
	
    public function __construct() {
        
		parent:: __construct();
        
        $this->config->load('platform', TRUE);
        $this->platform_config    =  $this->config->item('platform');
        $this->config->load('transactions', TRUE);
        $this->transactions_config = $this->config->item('transactions');
        $this->load->helper('recurive_array_search');
        $this->load->model('trace_model');
        
	}

    public function index () {    
    
        $this->load->view('template/header_view', $this->transactions_config);
        $this->load->view('template/side_bar_view');
        
        foreach ($this->transactions_config['function_config'] as $data) {       

            $this->load->view('trace_tab/trace_tab_view', $data);
            
        }
        
        $this->load->view('template/footer_view');
        
    }

    public function get_errors_by_carrier ($data) {
       
        $data = $this->transactions_config['function_config'][$data];
        $this->load->view('template/header_view', $this->transactions_config);
        $this->load->view('template/side_bar_view');
        
        $this->load->view('trace_tab/trace_tab_view', $data);
        $this->load->view('template/footer_view');
               
    }
            // TODO quitar las fechas hardcodeadas 
    public function get_errors () {
        
        $data['array_index'] = $this->input->get('array_index', FALSE);
        $data = $this->transactions_config['function_config'][$data['array_index']];
        $data['date_from']  = ($this->input->get('date_from', FALSE)) ? $this->input->get('date_from', FALSE) : '01/07/2015';
        $data['date_to'] = ($this->input->get('date_to', FALSE)) ? $this->input->get('date_to', FALSE) : '15/07/2015'; 
        
        $this->session->set_userdata($data['array_index'].'_date_from', $data['date_from']);
        $this->session->set_userdata($data['array_index'].'_date_to', $data['date_to']);
        //$this->session->sess_destroy();
        $get_errors_result = $this->trace_model->$data['function_query']($data);

        echo $get_errors_result;
        
    }
    
    public function get_filtered_totals() {
        
        $data['array_index'] = $this->input->get('array_index', FALSE);
        $data = $this->transactions_config['function_config'][$data['array_index']];
        $data['date_from']  = ($this->input->get('date_from', FALSE)) ? $this->input->get('date_from', FALSE) : '01/07/2015';
        $data['date_to'] = ($this->input->get('date_to', FALSE)) ? $this->input->get('date_to', FALSE) : '15/07/2015'; 
        $filtered_totals_query = $data['function_query'].'_totals';
        
        $total_records = $this->trace_model->$filtered_totals_query($data);
        $result = $this->parse_response($data, $total_records);
       
        echo $result;
        
    }
    
    private function parse_response ($data, $total_records) {
        
        $data['total_filtered'] =0;
        $data['total_not_filtered'] = 0;
        $array_result = json_decode($total_records);
        

        foreach ( $array_result->data as $key  ) {
            
            $data['total_filtered'] += $key->COUNT_FILTERED;
            $data['total_not_filtered'] += $key->COUNT_NOT_FILTERED;
            
        } 
        
        $this->session->set_userdata($data['array_index'].'_total_filtered', $data['total_filtered']);
        $this->session->set_userdata($data['array_index'].'_total_not_filtered', $data['total_not_filtered']);
        
        return json_encode($data);

    }
    
    public function get_row_error () {
        
        $data['array_index'] = $this->input->get_post('array_index', FALSE);
        $data = $this->transactions_config['function_config'][$data['array_index']];

        $data['row_date'] = $this->input->get_post('row_date', FALSE);
        $data['row_error'] = $this->input->get_post('row_error', FALSE);
        $data['url_json'] = 'get_row_errors_json';
        
        $this->load->view('template/header_view', $data);
        $this->load->view('template/side_bar_view',$this->transactions_config);
        $this->load->view('trace_tab/row_error_view');

    }
    
    public function get_row_errors_json () {
        
        $data['array_index'] = $this->input->get_post('array_index', FALSE);
        $data = $this->transactions_config['function_config'][$data['array_index']];

        $data['row_date'] = $this->input->get_post('row_date', FALSE);
        $data['row_error'] = $this->input->get_post('row_error', FALSE);
        
        if ($data['array_index'] == 'celcom') {

             echo $this->trace_model->get_celcom_row_errors_query($data);
            
        } else {
            
             echo $this->trace_model->get_row_errors_query($data);
            
        }
        
    }
     
}

/* End of file trace.php */
/* Location: ./application/controllers/trace.php */