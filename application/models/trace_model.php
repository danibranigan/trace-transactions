<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trace_model extends My_Controller {
    
    private $alias;
    
    public function __construct() {
        
		parent:: __construct();
        
        $this->load->library('datatables');
        
	}
    
    function get_row_errors_query ($data) {
        
        $bd_table = $data['bd_table'];
        $id_carrier = $data['id_carrier'];
        $row_date = $data['row_date'];
        $row_error = $data['row_error'];
        $where_date = $data['where_date'];
        $error_row_name = $data['error_row_name'];
        
        $this->datatables->select( $data['transactions_errors_columns'], false);
        $this->datatables->from($data['bd_table']);
        $this->datatables->join("transactions_carrier_codes", "  \"transactions_carrier_codes.carrier_code = $bd_table.$error_row_name\" AND \"transactions_carrier_codes\".\"id_carrier\"  =  '$id_carrier' ", 'left', false);
        
        $this->datatables->join("service", " service.id = to_char(\"$bd_table\".\"id_service\")  ", 'left', true);
        $this->datatables->join("methods", " methods.id = to_char(\"$bd_table\".\"id_method\")  ", 'left', true);
        $this->datatables->join("events", " events.id = to_char(\"$bd_table\".\"id_event\")  ", 'left', true);
        $this->datatables->where(" $where_date =", " '$row_date' ", false);
        $this->datatables->where(" \"$error_row_name\" = ", " '$row_error' ", false);
        $this->datatables->group_by($data['transactions_errors_columns_goup_by']);

        $json =  $this->datatables->generate();
        return $json;
        
    }
    
     function get_celcom_row_errors_query ($data) {
        
        $bd_table = $data['bd_table'];
        $id_carrier = $data['id_carrier'];
        $row_date = $data['row_date'];
        $row_error = $data['row_error'];
        $where_date = $data['where_date'];
        $error_row_name = $data['error_row_name'];
        
        $this->datatables->select( $data['transactions_errors_columns'], false);
        $this->datatables->from($data['bd_table']);
        $this->datatables->join("transactions_carrier_codes", "  \"transactions_carrier_codes.carrier_code = $bd_table.$error_row_name\" AND \"transactions_carrier_codes\".\"id_carrier\"  =  '$id_carrier' ", 'left', false);
        $this->datatables->join("events", " events.id = to_char(\"$bd_table\".\"id_event\")  ", 'left', true);
        $this->datatables->where(" $where_date =", " '$row_date' ", false);
        $this->datatables->where(" \"$error_row_name\" = ", " '$row_error' ", false);
        $this->datatables->group_by($data['transactions_errors_columns_goup_by']);

        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_celcom_query ($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
                trunc("created_at"), 
                "notif_code", 
                "transactions_carrier_codes"."description", 
                count("notif_code"),
                (
                    SELECT 
                        count("notif_code")
                    FROM "transactions_celcom"
                    WHERE "created_at" >= to_date(\''.$date_from.'\',\'dd/mm/yyyy\')
                    AND "created_at" <= to_date(\''.$date_to.'\',\'dd/mm/yyyy\')
                )total_filtered
                ', false);
        $this->datatables->from('transactions_celcom');
        $this->datatables->join('transactions_carrier_codes', '  transactions_celcom.notif_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 7 ', 'left', false);
        $this->datatables->where('notif_code !=', 'Success');
        $this->datatables->where('"transactions_carrier_codes"."id_carrier" =', 7, false);
        $this->datatables->where(' "created_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "created_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"created_at\")", "notif_code", "transactions_carrier_codes\".\"description"));
        
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_celcom_query_totals($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
               count(CASE WHEN "notif_code" != \'Success\' THEN "notif_code" ELSE null END) count_filtered, count(*) count_not_filtered
                ', false);
        $this->datatables->from('transactions_celcom');
        $this->datatables->join('transactions_carrier_codes', '  transactions_celcom.notif_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 7 ', 'left', false);
        $this->datatables->where('"transactions_carrier_codes"."id_carrier" =', 7, false);
        $this->datatables->where(' "created_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "created_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"created_at\")", "notif_code", "transactions_carrier_codes\".\"description"));
        
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function kannel_trace_query ($data) {
        
        $time_from = strtotime($data['date_from']);
        $time_to = strtotime($data['date_to']);

        $date_from = date('d-m-Y',$time_from);
        $date_to = date('d-m-Y',$time_to);
        
        $this->datatables->select(' trunc("date_request"), "shortcode", "transactions_carrier_codes"."description" , count("shortcode")  ', false);
        $this->datatables->from('transactions_kannel');
        $this->datatables->join('transactions_carrier_codes', 'transactions_carrier_codes.carrier_code = transactions_kannel.response', 'left');
        $this->datatables->where('shortcode !=', '0: Accepted for delivery');
        $this->datatables->where('shortcode !=', '3: Queued for later delivery');
        $this->datatables->where(' "shortcode" !=',  " '7' " , false);
        $this->datatables->where('"transactions_carrier_codes"."id_carrier" =', 12345, false);
        $this->datatables->where(' "date_request" >= ', " to_date('$date_from','dd/mm/yyyy ') ",false);
        $this->datatables->where(' "date_request" <= ', "to_date('$date_to','dd/mm/yyyy ')",false);
        $this->datatables->group_by(array( "trunc(\"date_request\")", "shortcode", "transactions_carrier_codes\".\"description"));

        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_my_umobile_query ($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
                trunc("created_at"), 
                "id_carrier_response_code", 
                "transactions_carrier_codes"."description", 
                count("id_carrier_response_code"),
                (
                    SELECT 
                        count("id_carrier_response_code")
                    FROM "transactions_my_umobile"
                    WHERE "created_at" >= to_date(\''.$date_from.'\',\'dd/mm/yyyy\')
                    AND "created_at" <= to_date(\''.$date_to.'\',\'dd/mm/yyyy\')
                )total_filtered
                ', false);
        $this->datatables->from('transactions_my_umobile');
        $this->datatables->join('transactions_carrier_codes', ' transactions_my_umobile.id_carrier_response_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 617', 'left', false);
        $this->datatables->where('id_carrier_response_code !=', 1);
        $this->datatables->where(' "created_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "created_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"created_at\")", "id_carrier_response_code", "transactions_carrier_codes\".\"description"));
        //echo $this->datatables->get_last_query();die();
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_my_umobile_query_totals($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
               count(CASE WHEN "id_carrier_response_code" != 1 THEN "id_carrier_response_code" ELSE null END) count_filtered, count(*) count_not_filtered
                ', false);
        $this->datatables->from('transactions_my_umobile');
        $this->datatables->join('transactions_carrier_codes', ' transactions_my_umobile.id_carrier_response_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 617', 'left', false);
        $this->datatables->where(' "created_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "created_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"created_at\")", "id_carrier_response_code", "transactions_carrier_codes\".\"description"));
        //echo $this->datatables->get_last_query();die();
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_rs_vip_query ($data) {

        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
                trunc("request_at"), 
                "response_code", 
                "transactions_carrier_codes"."description", 
                count("response_code"),
                (
                    SELECT 
                        count("response_code")
                    FROM "transactions_rs_vip"
                    WHERE "request_at" >= to_date(\''.$date_from.'\',\'dd/mm/yyyy\')
                    AND "request_at" <= to_date(\''.$date_to.'\',\'dd/mm/yyyy\')
                )total_filtered
                ', false);
        $this->datatables->from('transactions_rs_vip');
        $this->datatables->join('transactions_carrier_codes', ' transactions_rs_vip.response_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 480', 'left', false);
        $this->datatables->where('response_code !=', 200);
        $this->datatables->where(' "request_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "request_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"request_at\")", "response_code", "transactions_carrier_codes\".\"description"));
       
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_rs_vip_query_totals($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
               count(CASE WHEN "response_code" != 200 THEN "response_code" ELSE null END) count_filtered, count(*) count_not_filtered
                ', false);
        $this->datatables->from('transactions_rs_vip');
        $this->datatables->join('transactions_carrier_codes', ' transactions_rs_vip.response_code = transactions_carrier_codes.carrier_code AND "transactions_carrier_codes"."id_carrier" = 480', 'left', false);
        $this->datatables->where(' "request_at" >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' "request_at" <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"request_at\")", "response_code", "transactions_carrier_codes\".\"description"));
       
        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_ru_mts_query ($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];

        $this->datatables->select(' 
                trunc("created_at"), 
                "error_code", 
                "transactions_carrier_codes"."description", 
                count("error_code")
                ', false); 
        $this->datatables->from('transactions_ru_mts');
        $this->datatables->join('transactions_carrier_codes', ' transactions_ru_mts.error_code = transactions_carrier_codes.carrier_code  AND "transactions_carrier_codes"."id_carrier" = 573', 'left', false);
        $this->datatables->where('error_code IS NOT NULL');
        $this->datatables->where(' trunc("created_at") >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' trunc("created_at") <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "trunc(\"created_at\")", "error_code", "transactions_carrier_codes\".\"description"));
        
       //echo $this->datatables->get_last_query();

        $json =  $this->datatables->generate();
        return $json;
        
    }
    
    function trace_ru_mts_query_totals($data) {
        
        $date_from = $data['date_from'];
        $date_to = $data['date_to'];
        
        $this->datatables->select(' 
               count(CASE WHEN "error_code" is not null THEN "error_code" ELSE null END) count_filtered, count(*) count_not_filtered
                ', false);
        $this->datatables->from('transactions_ru_mts');
        $this->datatables->join('transactions_carrier_codes', ' transactions_ru_mts.error_code = transactions_carrier_codes.carrier_code  AND "transactions_carrier_codes"."id_carrier" = 573', 'left', false);
        $this->datatables->where(' trunc("created_at") >= ', " to_date('$date_from','dd/mm/yyyy') ",false);
        $this->datatables->where(' trunc("created_at") <= ', "to_date('$date_to','dd/mm/yyyy')",false);
        $this->datatables->group_by(array( "to_char(\"created_at\",'dd/mm/yyyy')", "error_code", "transactions_carrier_codes\".\"description"));
        
       //echo $this->datatables->get_last_query();

        $json =  $this->datatables->generate();
        return $json;
        
    }

}

/* Location: ./application/controllers/trace_model.php */