<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class My_Controller extends CI_Controller {
    
    protected function load_views ($route, $data) {
        
        $urlArray                                = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $segments                             = explode('/', $urlArray);
        $numSegments                      = count($segments);
        $data['currentSegment']         = $segments[$numSegments - 2] . '/' . $segments[$numSegments - 1];
        
        $this->load->view('template/header_view', $data);
        $this->load->view('template/side_bar_view');
        //$this->load->view($route);
        //$this->load->view('template/footer_view');
        
    }
    

    
    
}

?>