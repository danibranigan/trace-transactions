<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['function_config']	= array(

   // Needs to fix the transaction table
/*    'hr_tmobile' => array(
                        'table_name' => 'table-hr_tmobile',
                        'daterange_name' => 'hr_tmobile_daterange_name',
                        'title' => 'Tmobile (hr)',
    					'function_name' => 'hr_tmobile_json', // May not be needed
    					'function_query' => 'trace_hr_tmobile_query',
                        'bd_table' => 'transactions_hr_tmobile',
                        // TODO platform_code should be changed when the table is updated with carriers error
    					'table_columns' => 'created_at, platform_code, id_service, id_method',
    					'id_services' => array(1002)
    
    ), */
    
     // Needs to fix the transaction table
    /* 'id_indosat' => array(
    
                    'table_name' => 'table-id_indosat',
                    'daterange_name' => 'id_indosat_daterange_name',
                    'title' => 'Indosat (id)',
					'function_name' => 'id_indosat_json', // May not be needed
					'function_query' => 'trace_id_indosat_query',
                    'bd_table' => 'transactions_id_indosat',
                    // TODO platform_code should be changed when the table is updated with carriers error
					'table_columns' => 'created_at, carrier_status, id_service, id_method',
					'id_services' => array(1022)
    
    ),
    
    'it_mobilepay' => array(
    
                    'table_name' => 'table-it_mobilepay',
                    'daterange_name' => 'it_mobilepay_daterange_name',
                    'title' => 'Mobilepay (it)',
					'function_name' => 'it_mobilepay_json', // May not be needed
					'function_query' => 'trace_it_mobilepay_query',
                    'bd_table' => 'transactions_it_mobilepay',
                    // TODO platform_code should be changed when the table is updated with carriers error
					'table_columns' => 'created_at, result_code, id_service, id_method',
					'id_services' => array(1043, 1176)
    
    ), */
    
/*     'kannel' => array(
					
                    'array_index' => 'kannel',                                             // This is to get the array index for each table
                    'table_name' => 'table-kannel',                                    //  The datatable name
                    'daterange_name' => 'kannel_daterange_name',         // Name of the JQuery date selector, each one has to be different 
                    'title' => 'Kannel',                                                        // The title for the view in <h1> 
					'function_name' => 'kannel_json',                               // May not be needed
					'function_query' => 'kannel_trace_query',                   //  Not needed, in case carrier needs a special function
                    'bd_table' => 'transactions_kannel',                            //  This is the transactions table name
					'table_columns' => 'created_at, response',                 //  Columns in the select to get all errors in the main view 
					'error_row_name' => 'response',                               // The columns that has the carrier error in the transactions carriers table
                    'where_date' => ' trunc("date_request") ',                                          // Date to be used in the where clause
                    'transactions_errors_columns' => ' "id_transaction_target", trunc("date_request"), "id_service", "id_event", "id_method", "shortcode", count("response")' , // The columns in the select to get a specific error for row_errors view
					'id_carrier' => '12345',                                              // id_carrier
					'id_services' => array(844)                                      // Not needed, the carriers services
					
					),  */
                    
    'celcom' => array(
					
                    'array_index' => 'celcom',
                    'table_name' => 'table-celcom',
                    'daterange_name' => 'celcom_daterange_name',
                    'title' => 'Celcom',
					'function_name' => 'celcom_json', // May not be needed
					'function_query' => 'trace_celcom_query',
                    'bd_table' => 'transactions_celcom',
					'table_columns' => 'created_at, notif_code',
                    'error_row_name' => 'notif_code',
                    'where_date' => ' trunc("date_call") ', 
                    'transactions_errors_columns' => ' trunc("date_call"), "notif_code", "event_name", "url_call", "notif_message", "transactions_celcom"."id" ',
                    'transactions_errors_columns_goup_by' => array( "trunc(\"date_call\")", "notif_code", "event_name", "url_call", "notif_message", "transactions_celcom\".\"id"),
					'id_carrier' => '7',
					//'id_services' => array(844)
					
					),
    
    'my_umobile' => array(
					
                    'array_index' => 'my_umobile',                                                       
                    'table_name' => 'table-my_umobile',                                              
                    'daterange_name' => 'my_umobile_daterange_name',                    
                    'title' => 'Umobile (my)',                                                                 
					'function_name' => 'my_umobile_json',     
					'function_query' => 'trace_my_umobile_query',                              
                    'bd_table' => 'transactions_my_umobile',                                        
					'table_columns' => 'created_at, id_carrier_response_code',            
                    'error_row_name' => 'id_carrier_response_code',                        
                    'where_date' => ' trunc("created_at") ',                                          
                    'transactions_errors_columns' => ' trunc("created_at"), "id_carrier_response_code", "event_name", "service_name", "methods"."type", "request", "response",  "transactions_my_umobile"."id"',
                    'transactions_errors_columns_goup_by' => array( "trunc(\"created_at\")", "id_carrier_response_code", "event_name", "service_name", "methods\".\"type", "id_method", "request", "response",  "transactions_my_umobile\".\"id"),
					'id_carrier' => '617',
					'id_services' => array(844)
					
					),
                    
    'rs_vip' => array(
					
                    'array_index' => 'rs_vip',
                    'table_name' => 'table-rs_vip',
                    'daterange_name' => 'rs_vip_daterange_name',
                    'title' => 'VIP (rs)',
					'function_name' => 'rs_vip_json', 
					'function_query' => 'trace_rs_vip_query',
                    'bd_table' => 'transactions_rs_vip',
					'table_columns' => 'created_at, response_code',
                    'error_row_name' => 'response_code',
                    'where_date' => ' trunc("request_at") ', 
                    'transactions_errors_columns' => ' trunc("request_at"), "response_code","event_name", "service_name", "methods"."type", "data", "response_description",  "transactions_rs_vip"."id" ',
                    'transactions_errors_columns_goup_by' => array( "trunc(\"request_at\")", "response_code", "event_name", "service_name", "data", "methods\".\"type","response_description", "transactions_rs_vip\".\"id"),
                    'id_carrier' => '480',
					//'id_services' => array(844)
					
					),

    'ru_mts' => array(
					
                    'array_index' => 'ru_mts',
                    'table_name' => 'table-ru_mts',
                    'daterange_name' => 'ru_mts_daterange_name',
                    'title' => 'MTS (ru)',
					'function_name' => 'ru_mts_json', 
					'function_query' => 'trace_ru_mts_query',
                    'bd_table' => 'transactions_ru_mts',
					'table_columns' => 'created_at, error_code',
                    'error_row_name' => 'error_code',
                    'where_date' => ' trunc("created_at") ', 
                    'transactions_errors_columns' => ' trunc("created_at"), "error_code", "event_name", "service_name", "methods"."type", "request", "response", "transactions_ru_mts"."id" ',
                    'transactions_errors_columns_goup_by' => array( "trunc(\"created_at\")", "error_code", "event_name", "service_name", "request", "methods\".\"type", "response", "transactions_ru_mts\".\"id" ),
					'id_carrier' => '573',
					//'id_services' => array(844)
					
					)
     
);
/* End of file */
