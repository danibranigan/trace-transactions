<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// USER STATUS TYPES
$config['user_not_exist']   = 0;
$config['user_pre_added']   = 1;
$config['user_abort']       = 2;
$config['user_active']      = 3;
$config['user_inactive']    = 4;
$config['user_ex_client']   = 5;
$config['user_blacklist']   = 6;
$config['user_suspended']   = 7;

// SUBSCRIBER STATUS TYPES
$config['subs_inactive']    = 0;
$config['subs_active']      = 1;
$config['subs_ex_client']   = 2;
$config['subs_pending_dlr'] = 3;
$config['subs_suspended']   = 4;
$config['subs_waiting_pin'] = 5;

// EVENT TYPES
$config['optin']                 =  1;
$config['optout']                =  2;
$config['retry']                 =  3;
$config['retry_partial']         =  4;
$config['rebill']                =  5;
$config['rebill_partial']        =  6;
$config['bill_download']         =  7;
$config['bill_download_premium'] =  8;
$config['credit_buy']            =  9;
$config['pre_add']               = 10;
$config['abort']                 = 11;
$config['blacklist']             = 12;
$config['attempt']               = 14;
$config['info']                  = 15;
$config['suspend']               = 16;
$config['resume']                = 17;
$config['confirm']               = 18;
$config['grace_period']          = 19;
$config['free_mt_now']           = 20;
$config['free_mt_delayed']       = 21;
$config['first_bill']            = 22;
$config['duplicated_request']    = 24;
$config['credit_discount']       = 25;
$config['billed_mo']             = 26;

//MICRO EVENTS
$config['me_welcome']               = 100;
$config['me_first_bill']            = 101;
$config['me_already']               = 102;
$config['me_rebill']                = 103;
$config['me_help']                  = 104;
$config['me_optout']                = 105;
$config['me_infocredits']           = 106;
$config['me_send_pin']              = 107;
$config['me_invalid']               = 108;
$config['me_not_subscribed']        = 109;
$config['me_reminder']              = 110;
$config['me_stop_shortcode']        = 111;
$config['me_stop_carrier']          = 112;
$config['me_send_confirm']          = 113;
$config['me_stop_one_or_send_list'] = 114;
$config['me_no_funds_subscribe']    = 115;
$config['me_post_rebill_sms']       = 116;
$config['me_direct_bill']           = 117;
$config['me_content']               = 118;
$config['me_reminder']              = 119;
$config['me_send_service_list']     = 120;
$config['me_confirm']               = 121;
$config['me_after_first_bill']      = 200;
$config['me_after_rebill']          = 201;
$config['me_after_invalid']         = 202;

$config['me_pre_preadd']            = 500;
$config['me_post_preadd']           = 501;
$config['me_pre_subscribe']         = 502;
$config['me_post_subscribe']        = 503;
$config['me_pre_unsubscribe']       = 504;
$config['me_post_unsubscribe']      = 505;
$config['me_redirect_request']      = 510;
$config['me_sms_games_info']        = 511;
$config['me_sms_games_help']        = 512;
$config['me_service_list']          = 520;

//ME GROUPS
$config['me_groups']['hooks']['start']  = 500;
$config['me_groups']['hooks']['end']    = 599;


// METHODS
$config['wap_method'] = 1;
$config['web_method'] = 2;
$config['sms_method'] = 3;
$config['int_method'] = 4;
$config['ext_method'] = 5;

// BILL EVENT RESULTS
$config['event_billed'] = 1;
$config['event_not_billed'] = 0;

// USER FUNDS
$config['user_had_no_funds'] = 0;
$config['user_had_funds']    = 1;

// RESPONSE CODES
$config['not_confirmed']                = 0;
$config['success']                      = 100;
$config['in_process']                   = 101;
$config['already_preadded']             = 102;
$config['no_funds']                     = 200;
$config['blacklisted']                  = 800;
$config['error']                        = 900;
$config['error_recover']                = 901;
$config['error_already_active']         = 902;
$config['bill_steps_finished']          = 903;
//NEW RESPONSE CODES
$config['subscription_async_result']    = 102;
$config['generic_parameter_error']      = 1100;
$config['billing_parameter_error']      = 1101;
$config['auth_parameter_error']         = 1102;
$config['smsc_parameter_error']         = 1103;
$config['service_parameter_error']      = 1104;
$config['generic_connectiviy_error']    = 1200;
$config['connection_timeout']           = 1201;
$config['connection_refused']           = 1202;
$config['certificates_error']           = 1203;
$config['connection_busy']              = 1205;
$config['vpn_error']                    = 1204;
$config['generic_user_error']           = 1300;
$config['user_is_not_customer']         = 1301;
$config['user_age_not_allowed']         = 1302;
$config['unknown_user_error']           = 1303;
$config['blocked_user']                 = 1304;
$config['suspended_user']               = 1305;
$config['premium_not_allowed']          = 1306;
$config['invalid_msisdn']               = 1307;
$config['user_already_sub_in_carrier']  = 1308;
$config['user_sync_problem']            = 1309;
$config['user_barred']                  = 1310;
$config['invalid_pin']                  = 1311;
$config['user_was_active']              = 1312;
$config['generic_limit_error']          = 1400;
$config['bill_amount_out_range']        = 1401;
$config['limit_trans_second']           = 1402;
$config['limit_trans_hour']             = 1403;
$config['limit_trans_daily']            = 1404;
$config['limit_trans_period']           = 1405;
$config['multiple_trans_not_allowed']   = 1406;
$config['limit_sms_exceeded']           = 1407;
$config['generic_duplicity_error']      = 1500;
$config['trans_already_exists']         = 1501;
$config['generic_carrier_error']        = 1600;
$config['rejected_by_carrier']          = 1601;
$config['internal_error_carrier']       = 1602;
$config['bad_response_carrier']         = 1603;
$config['undefined_error_code_carrier'] = 1604;
$config['generic_error']                = 1700;
$config['unknown_error']                = 1701;


// SMS POLICY
$config['no_match_need']      = 1;
$config['exact_match_need']   = 2;
$config['partial_match_need'] = 3;

// SMSC TYPES & FLOWS
$config['id_type_smsc_billed'] = 1;
$config['id_type_smsc_free']   = 2;

$config['id_flow_smsc_in']     = 1;
$config['id_flow_smsc_out']    = 2;
$config['id_flow_smsc_in_out'] = 3;


// SERVICES
$config['my_timwe']            = 8;
$config['my_celcom']           = 50;
$config['cz_tmobile']          = 9;
$config['cz_tmobile_exoticas'] = 82;
$config['ec_cnt_ondemand']     = 122;
$config['iq_asiacell_english'] = 206;
$config['iq_asiacell_arabic']  = 222;
$config['iq_zain_english']     = 142;
$config['iq_zain_arabic']      = 144;
$config['ng_etisalat_daily']   = 223;
$config['ng_etisalat_weekly']  = 224;
$config['ng_etisalat_monthly'] = 225;
$config['ni_claro_clubbinbit'] = 242;
$config['ni_claro_clubenter']  = 243;
$config['ni_claro_clubgoool']  = 244;
$config['ru_beeline_bb']       = 402;
$config['ru_beeline_km']       = 482;
$config['ru_beeline_km_2']     = 764;
$config['pt_meo']              = 722;
$config['es_yoigo']            = (ENVIRONMENT == 'production') ? 622 : 607;
$config['ru_mts_bb']           = (ENVIRONMENT == 'production') ? 682 : 627;
$config['ru_mts_km']           = (ENVIRONMENT == 'production') ? 683 : 628;
$config['hr_tele2_games']      = 748;
$config['hr_tele2_apps']       = 747;
$config['hr_tele2_music']      = 1144;
$config['hr_tele2_maxim']      = 1143;
$config['hr_tele2_bcn']        = 1142;
$config['ng_globacom_ef_daily']= (ENVIRONMENT == 'production') ? 913 : 819;
$config['ph_smart']            = (ENVIRONMENT == 'production') ? 751 : 751;
$config['mobilepay']           = (ENVIRONMENT == 'production') ? 1042: 649;
$config['it_h3g']              = (ENVIRONMENT == 'production') ? 1043: 968;
$config['it_fastweb']          = 1167;
$config['it_tim']              = 1175;
$config['it_vodafone']         = 1176;
$config['it_wind']             = 1177;
$config['it_poste']            = 1178;
$config['cdc']                 = array(
    1082, 1083, 1084, 1085, // HONDURAS - MM

    1162,1163,1164,1168,1169,1170,1171,1172,1173,1223,1224,1225, // EL SALVADOR - MM
    1182,1183,1184,1185,1186,1187,1188,1189,1190,1192,1193,1194,1195,1196,1197,1198,1199,1200,1201,1202,1203,1204,1205,1206,1207,1208,1209,1210,1211,1212, // EL SALVADOR - TEXTO

    1262,1263,1318,1316,1308,1306,1287,1242,1286,1285,1284,1283,1264,1282,  // GUATEMALA - MM
    1304,1303,1313,1312,1311,1319,1309,1315,1314,1305,1310, // GUATEMALA - TEXTO

    1302,1410,1411,1412,1413,1414,1415,1416, // NICARAGUA - MM
    1322,1323,1324,1325,1326,1327,1328,1329,1330,1331,1332,1333,1342,1343,1344,1345,1346,1347,1348,1349,1350,1351,1352,1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1402,1403,1404,1405,1406,1407,1408,1409, //NICARAGUA - TEXTO
);

// CLIENT ID
$config['client_binbit']   = 1;
$config['client_kitmaker'] = 2;
$config['client_tele2']    = 3;

// CARRIERS EXCEPTIONS
$config['carrier_exceptions'] = array(
    520, 41302, // DIALOG - SRI LANKA
    2, 732101, // CLARO - CO
);

$config['subscribe_type']            = 1;
$config['ondemand_type']             = 2;
$config['text_type']                 = 3;
$config['combo_type']                = 4;
$config['combo_extra_type']          = 5;
$config['subscription_credits_type'] = 6;
$config['sms_game']                  = 7;

// SERVICE MODE
$config['active_mode']  = 1;
$config['passive_mode'] = 2;

// TRANSACTION TARGET
$config['target_internal'] = 1;
$config['target_external'] = 2;

// PRICING METHODS
$config['pricing_by_code'] = 1;
$config['pricing_by_price'] = 2;

// PRICE CODE TYPES
$config['price_free']         = 1;
$config['price_bill']         = 2;
$config['price_bill_partial'] = 3;
$config['price_bill_credits'] = 4;

// KEYWORD TYPES (sms_keyword_type table)
$config['keyword_unknown']     = 0;
$config['keyword_optin']       = 1;
$config['keyword_optout']      = 2;
$config['keyword_info']        = 3;
$config['keyword_confirm']     = 4;
$config['keyword_event_based'] = 5;

// SCHEDULES TYPES
$config['task_by_amount']       = 61;
$config['task_reset_amount']    = 62;

// PIN CODES STATUS
$config['pin_code_active']    = 1;
$config['pin_code_confirmed'] = 2;
$config['pin_code_expired']   = 3;


// PRODUCTION
$config['combo_services'] = array (356, 355, 354, 353, 352, 351, 350, 349, 348, 347, 346, 357);
$config['exoticas_text_services'] = array(292, 294, 299, 310, 313, 314);
$config['binbit_text_services']   = array(282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 293, 295, 296, 297, 298, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 311, 312, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327);
$config['shortcode_unsubscribe_services'] = array(787,788,789,790,791,792,793,794,796,798,799,800,802,803,804,805,806,807,809,810,811,813,815,816,817,818,819,820,821,822,922,923,924);

// -----------------------------------------------------------------
// NEW PLATFORM RESPONSE CODES
// -----------------------------------------------------------------

// -----------------------------------------------------------------
// SUCCESS CODES code_ext
// -----------------------------------------------------------------
$config['platform_codes'][100]['code']             = 100;
$config['platform_codes'][100]['detail']           = 100;
$config['platform_codes'][100]['msg']              = "success";
$config['platform_codes'][100]['level']            = "info";
$config['platform_codes'][100]['action_triggered'] = "";

$config['platform_codes'][101]['code']             = 100;
$config['platform_codes'][101]['detail']           = 101;
$config['platform_codes'][101]['msg']              = "in_process";
$config['platform_codes'][101]['level']            = "info";
$config['platform_codes'][101]['action_triggered'] = "";

$config['platform_codes'][102]['code']             = 100;
$config['platform_codes'][102]['detail']           = 102;
$config['platform_codes'][102]['msg']              = "subscription async result";
$config['platform_codes'][102]['level']            = "info";
$config['platform_codes'][102]['action_triggered'] = "";
// -----------------------------------------------------------------
// NO FUNDS
// -----------------------------------------------------------------
$config['platform_codes'][200]['code']             = 200;
$config['platform_codes'][200]['detail']           = 200;
$config['platform_codes'][200]['msg']              = "subscriber without funds";
$config['platform_codes'][200]['level']            = "error";
$config['platform_codes'][200]['action_triggered'] = "partialbill-or-retry";
// -----------------------------------------------------------------
// BLACKLIST
// -----------------------------------------------------------------
$config['platform_codes'][800]['code']             = 800;
$config['platform_codes'][800]['detail']           = 800;
$config['platform_codes'][800]['msg']              = "blacklisted user";
$config['platform_codes'][800]['level']            = "error";
$config['platform_codes'][800]['action_triggered'] = "blacklist";
// -----------------------------------------------------------------
// GENERIC PLATFORM ERRORS
// -----------------------------------------------------------------
$config['platform_codes'][900]['code']             = 900;
$config['platform_codes'][900]['detail']           = 900;
$config['platform_codes'][900]['msg']              = "generic platform error";
$config['platform_codes'][900]['level']            = "error";
$config['platform_codes'][900]['action_triggered'] = "";
// -----------------------------------------------------------------
// GENERIC PLATFORM RECOVERABLE ERRORS
// -----------------------------------------------------------------
$config['platform_codes'][901]['code']             = 901;
$config['platform_codes'][901]['detail']           = 901;
$config['platform_codes'][901]['msg']              = "generic platform recoverable error";
$config['platform_codes'][901]['level']            = "error";
$config['platform_codes'][901]['action_triggered'] = "";
// -----------------------------------------------------------------
// PARAMETER ERRORS (1100-1199 RESERVED)
// -----------------------------------------------------------------
$config['platform_codes'][1100]['code']             = 900;
$config['platform_codes'][1100]['detail']           = 1100;
$config['platform_codes'][1100]['msg']              = "generic parameter error";
$config['platform_codes'][1100]['level']            = "error";
$config['platform_codes'][1100]['action_triggered'] = "";

$config['platform_codes'][1101]['code']             = 900;
$config['platform_codes'][1101]['detail']           = 1101;
$config['platform_codes'][1101]['msg']              = "billing parameter error";
$config['platform_codes'][1101]['level']            = "error";
$config['platform_codes'][1101]['action_triggered'] = "";

$config['platform_codes'][1102]['code']             = 900;
$config['platform_codes'][1102]['detail']           = 1102;
$config['platform_codes'][1102]['msg']              = "auth parameter error";
$config['platform_codes'][1102]['level']            = "error";
$config['platform_codes'][1102]['action_triggered'] = "";

$config['platform_codes'][1103]['code']             = 900;
$config['platform_codes'][1103]['detail']           = 1103;
$config['platform_codes'][1103]['msg']              = "smsc parameter error";
$config['platform_codes'][1103]['level']            = "error";
$config['platform_codes'][1103]['action_triggered'] = "";

$config['platform_codes'][1104]['code']             = 900;
$config['platform_codes'][1104]['detail']           = 1104;
$config['platform_codes'][1104]['msg']              = "service parameter error";
$config['platform_codes'][1104]['level']            = "error";
$config['platform_codes'][1104]['action_triggered'] = "";
// -----------------------------------------------------------------
// CONNECTIVITY ERRORS (1200-1299 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1200]['code']             = 900;
$config['platform_codes'][1200]['detail']           = 1200;
$config['platform_codes'][1200]['msg']              = "generic connectivity error";
$config['platform_codes'][1200]['msg']              = "generic connectivity error";
$config['platform_codes'][1200]['level']            = "error";
$config['platform_codes'][1200]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1201]['code']             = 900;
$config['platform_codes'][1201]['detail']           = 1201;
$config['platform_codes'][1201]['msg']              = "connection timeout";
$config['platform_codes'][1201]['level']            = "error";
$config['platform_codes'][1201]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1202]['code']             = 900;
$config['platform_codes'][1202]['detail']           = 1202;
$config['platform_codes'][1202]['msg']              = "connection refused";
$config['platform_codes'][1202]['level']            = "error";
$config['platform_codes'][1202]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1203]['code']             = 900;
$config['platform_codes'][1203]['detail']           = 1203;
$config['platform_codes'][1203]['msg']              = "certificates error";
$config['platform_codes'][1203]['level']            = "error";
$config['platform_codes'][1203]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1204]['code']             = 900;
$config['platform_codes'][1204]['detail']           = 1204;
$config['platform_codes'][1204]['msg']              = "vpn error";
$config['platform_codes'][1204]['level']            = "error";
$config['platform_codes'][1204]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1205]['code']             = 900;
$config['platform_codes'][1205]['detail']           = 1205;
$config['platform_codes'][1205]['msg']              = "connection busy";
$config['platform_codes'][1205]['level']            = "error";
$config['platform_codes'][1205]['action_triggered'] = "interaction_retry_schedule";
// -----------------------------------------------------------------
// USER ERRORS (1300-1399 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1300]['code']             = 900;
$config['platform_codes'][1300]['detail']           = 1300;
$config['platform_codes'][1300]['msg']              = "generic user error";
$config['platform_codes'][1300]['level']            = "error";
$config['platform_codes'][1300]['action_triggered'] = "";

$config['platform_codes'][1301]['code']             = 900;
$config['platform_codes'][1301]['detail']           = 1301;
$config['platform_codes'][1301]['msg']              = "user is not a customer of the carrier";
$config['platform_codes'][1301]['level']            = "error";
$config['platform_codes'][1301]['action_triggered'] = "set_ex_client_user";

$config['platform_codes'][1302]['code']             = 900;
$config['platform_codes'][1302]['detail']           = 1302;
$config['platform_codes'][1302]['msg']              = "user age not allowed for this service";
$config['platform_codes'][1302]['level']            = "error";
$config['platform_codes'][1302]['action_triggered'] = "set_suspended_user";

$config['platform_codes'][1303]['code']             = 900;
$config['platform_codes'][1303]['detail']           = 1303;
$config['platform_codes'][1303]['msg']              = "unknown user error";
$config['platform_codes'][1303]['level']            = "error";
$config['platform_codes'][1303]['action_triggered'] = "";

$config['platform_codes'][1304]['code']             = 900;
$config['platform_codes'][1304]['detail']           = 1304;
$config['platform_codes'][1304]['msg']              = "blocked user";
$config['platform_codes'][1304]['level']            = "error";
$config['platform_codes'][1304]['action_triggered'] = "";

$config['platform_codes'][1305]['code']             = 900;
$config['platform_codes'][1305]['detail']           = 1305;
$config['platform_codes'][1305]['msg']              = "suspended user";
$config['platform_codes'][1305]['level']            = "error";
$config['platform_codes'][1305]['action_triggered'] = "set_suspended_user";

$config['platform_codes'][1306]['code']             = 900;
$config['platform_codes'][1306]['detail']           = 1306;
$config['platform_codes'][1306]['msg']              = "premium services not allowed for this user";
$config['platform_codes'][1306]['level']            = "error";
$config['platform_codes'][1306]['action_triggered'] = "set_suspended_user";

$config['platform_codes'][1307]['code']             = 900;
$config['platform_codes'][1307]['detail']           = 1307;
$config['platform_codes'][1307]['msg']              = "invalid msisdn";
$config['platform_codes'][1307]['level']            = "error";
$config['platform_codes'][1307]['action_triggered'] = "";

$config['platform_codes'][1308]['code']             = 900;
$config['platform_codes'][1308]['detail']           = 1308;
$config['platform_codes'][1308]['msg']              = "Sync problem, the user is already subscribed in third party system";
$config['platform_codes'][1308]['level']            = "error";
$config['platform_codes'][1308]['action_triggered'] = "";

$config['platform_codes'][1309]['code']             = 900;
$config['platform_codes'][1309]['detail']           = 1309;
$config['platform_codes'][1309]['msg']              = "Sync problem, the user has not synchronized properly";
$config['platform_codes'][1309]['level']            = "error";
$config['platform_codes'][1309]['action_triggered'] = "";

$config['platform_codes'][1310]['code']             = 900;
$config['platform_codes'][1310]['detail']           = 1310;
$config['platform_codes'][1310]['msg']              = "the user is temporarily barred";
$config['platform_codes'][1310]['level']            = "error";
$config['platform_codes'][1310]['action_triggered'] = "set_suspended_user";

$config['platform_codes'][1311]['code']             = 900;
$config['platform_codes'][1311]['detail']           = 1311;
$config['platform_codes'][1311]['msg']              = "invalid pin code";
$config['platform_codes'][1311]['level']            = "error";
$config['platform_codes'][1311]['action_triggered'] = "";

$config['platform_codes'][1312]['code']             = 900;
$config['platform_codes'][1312]['detail']           = 1312;
$config['platform_codes'][1312]['msg']              = "subscriber was active";
$config['platform_codes'][1312]['level']            = "info";
$config['platform_codes'][1312]['action_triggered'] = "";

// -----------------------------------------------------------------
// LIMITS ERRORS (1400-1499 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1400]['code']             = 900;
$config['platform_codes'][1400]['detail']           = 1400;
$config['platform_codes'][1400]['msg']              = "generic limit error";
$config['platform_codes'][1400]['level']            = "error";
$config['platform_codes'][1400]['action_triggered'] = "";

$config['platform_codes'][1401]['code']             = 900;
$config['platform_codes'][1401]['detail']           = 1401;
$config['platform_codes'][1401]['msg']              = "the bill amount is out of range";
$config['platform_codes'][1401]['level']            = "error";
$config['platform_codes'][1401]['action_triggered'] = "";

$config['platform_codes'][1402]['code']             = 900;
$config['platform_codes'][1402]['detail']           = 1402;
$config['platform_codes'][1402]['msg']              = "the limit of transactions per second was exceeded";
$config['platform_codes'][1402]['level']            = "error";
$config['platform_codes'][1402]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1403]['code']             = 900;
$config['platform_codes'][1403]['detail']           = 1403;
$config['platform_codes'][1403]['msg']              = "the limit of transactions per hour was exceeded";
$config['platform_codes'][1403]['level']            = "error";
$config['platform_codes'][1403]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1404]['code']             = 900;
$config['platform_codes'][1404]['detail']           = 1404;
$config['platform_codes'][1404]['msg']              = "the limit of daily transactions was exceeded";
$config['platform_codes'][1404]['level']            = "error";
$config['platform_codes'][1404]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1405]['code']             = 900;
$config['platform_codes'][1405]['detail']           = 1405;
$config['platform_codes'][1405]['msg']              = "the limit of transactions per period was exceeded";
$config['platform_codes'][1405]['level']            = "error";
$config['platform_codes'][1405]['action_triggered'] = "";

$config['platform_codes'][1406]['code']             = 900;
$config['platform_codes'][1406]['detail']           = 1406;
$config['platform_codes'][1406]['msg']              = "multiple user transactions are not allowed";
$config['platform_codes'][1406]['level']            = "error";
$config['platform_codes'][1406]['action_triggered'] = "";

$config['platform_codes'][1407]['code']             = 900;
$config['platform_codes'][1407]['detail']           = 1407;
$config['platform_codes'][1407]['msg']              = "the limit of sms was exceeded";
$config['platform_codes'][1407]['level']            = "error";
$config['platform_codes'][1407]['action_triggered'] = "interaction_retry_schedule";
// -----------------------------------------------------------------
// DUPLICITY ERRORS (1501 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1500]['code']             = 900;
$config['platform_codes'][1500]['detail']           = 1500;
$config['platform_codes'][1500]['msg']              = "generic duplicity error";
$config['platform_codes'][1500]['level']            = "error";
$config['platform_codes'][1500]['action_triggered'] = "";

$config['platform_codes'][1501]['code']             = 900;
$config['platform_codes'][1501]['detail']           = 1501;
$config['platform_codes'][1501]['msg']              = "transaction already exists";
$config['platform_codes'][1501]['level']            = "error";
$config['platform_codes'][1501]['action_triggered'] = "";
// -----------------------------------------------------------------
// THIRD PARTY SYSTEM ERRORS (1600-1699 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1600]['code']             = 900;
$config['platform_codes'][1600]['detail']           = 1600;
$config['platform_codes'][1600]['msg']              = "generic third party system error";
$config['platform_codes'][1600]['level']            = "error";
$config['platform_codes'][1600]['action_triggered'] = "";

$config['platform_codes'][1601]['code']             = 900;
$config['platform_codes'][1601]['detail']           = 1601;
$config['platform_codes'][1601]['msg']              = "rejected for third party system";
$config['platform_codes'][1601]['level']            = "error";
$config['platform_codes'][1601]['action_triggered'] = "";

$config['platform_codes'][1602]['code']             = 900;
$config['platform_codes'][1602]['detail']           = 1602;
$config['platform_codes'][1602]['msg']              = "third party internal system error";
$config['platform_codes'][1602]['level']            = "error";
$config['platform_codes'][1602]['action_triggered'] = "interaction_retry_schedule";

$config['platform_codes'][1603]['code']             = 900;
$config['platform_codes'][1603]['detail']           = 1603;
$config['platform_codes'][1603]['msg']              = "bad response from third party system";
$config['platform_codes'][1603]['level']            = "error";
$config['platform_codes'][1603]['action_triggered'] = "";

$config['platform_codes'][1604]['code']             = 900;
$config['platform_codes'][1604]['detail']           = 1604;
$config['platform_codes'][1604]['msg']              = "undefined error code from third party system";
$config['platform_codes'][1604]['level']            = "error";
$config['platform_codes'][1604]['action_triggered'] = "";
// -----------------------------------------------------------------
// GENERIC ERRORS (1700-1799 RESERVED)
// -----------------------------------------------------------------

$config['platform_codes'][1700]['code']             = 900;
$config['platform_codes'][1700]['detail']           = '';
$config['platform_codes'][1700]['msg']              = "generic error";
$config['platform_codes'][1700]['level']            = "error";
$config['platform_codes'][1700]['action_triggered'] = "";

$config['platform_codes'][1701]['code']             = 900;
$config['platform_codes'][1701]['detail']           = 1701;
$config['platform_codes'][1701]['msg']              = "unknown error";
$config['platform_codes'][1701]['level']            = "error";
$config['platform_codes'][1701]['action_triggered'] = "";

$config['platform_codes'][1702]['code']             = 900;
$config['platform_codes'][1702]['detail']           = 1702;
$config['platform_codes'][1702]['msg']              = "ignored_notification";
$config['platform_codes'][1702]['level']            = "error";
$config['platform_codes'][1702]['action_triggered'] = "";

/* End of file platform.php */
/* Location: ./application/config/platform.php */
