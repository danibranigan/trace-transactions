<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function recurive_array_search($needle, $haystack) {

    foreach($haystack as $key => $value) {

        if ($needle === $value || (is_array($value) && recurive_array_search($needle, $value) !== FALSE)) {
            return $key;
        }
    }

    return FALSE;

}