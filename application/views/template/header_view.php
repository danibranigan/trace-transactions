<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trace transactions</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css';?>" rel="stylesheet">
    
    <!-- DataTables CSS -->
    <!--link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/jquery.dataTables.css';?>"-->
    
    <!-- Bootstrap DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dataTables.bootstrap.css';?>">
      
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url().'assets/css/metisMenu.min.css';?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo base_url().'assets/css/timeline.css';?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/sb-admin-2.css';?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url().'assets/css/morris.css';?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/css/font-awesome.min.css';?>" rel="stylesheet" type="text/css">  
    
    <!-- Datapicker CSS  -->
    <link href="<?php echo base_url().'assets/css/datepicker3.css';?>" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.min.js';?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js';?>"></script>
    
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="<?php echo base_url().'assets/js/moment.js';?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'assets/js/daterangepicker.js';?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/daterangepicker.css';?>" />
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="<?php echo base_url().'assets/js/jquery.dataTables.js';?>"></script>
    
    <!--Bootstrap DataTables -->
    <script type="text/javascript" charset="utf8" src="<?php echo base_url().'assets/js/dataTables.bootstrap.js';?>"></script>
   

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url().'assets/js/metisMenu.min.js';?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url().'assets/js/sb-admin-2.js';?>"></script>    
    
    <!--Plugin Bootstrap Calendar -->
    <script src="<?php echo base_url().'assets/js/bootstrap-datepicker.min.js';?>"></script> 
    
    <!--Language spanish Bootstrap Calendar -->
    <script src="<?php echo base_url().'assets/js/dataPickerLang/bootstrap-datepicker.es.js';?>"></script> 

    <!--Language english Bootstrap Calendar -->
    <script src="<?php echo base_url().'assets/js/dataPickerLang/bootstrap-datepicker.en-GB.js';?>"></script>     

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
    
    .datatable-ellipse{
        
        max-width: 200px;
        min-width: 70px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        
    }
    
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>trace">KITMAKER</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

