            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                         
                        <li <?php $actual_url = explode("/", $_SERVER['REQUEST_URI']); if (empty($actual_url[3])) {  echo 'class=""'; } else { echo 'class="active"';} ?>>
                            <a href="#"><i class="fa fa-database fa-fw"></i> Transactions<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <?php foreach ($function_config as $transactions_data  => $value) { ?>
                                                                        
                                    <li>
                                        <a href="<?php echo base_url().'trace/get_errors_by_carrier/'.$transactions_data?>"><?php echo $value['title']; ?></a>
                                    </li>   
                                    
                                <?php } ?>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>


                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
         </nav>
                 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">