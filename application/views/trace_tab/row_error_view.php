

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><?php echo $title.' errors detail'; ?></h1>
                        
                        <div class="form-group">
                            <br>
                            <table id="<?php echo 'table_name'; ?>" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Created at</th>
                                        <th>Carrier response code</th>
                                        <th>Event</th>
                                        <th>Service</th>
                                        <th>Method</th>
                                        <th>Request</th>
                                        <th>Response</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                </tbody>   
                            </table>
                        </div>    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                


<script>

    $(document).ready(function() {
        
        window.onload = load_datatable();
     
        function load_datatable(date_from, date_to) {
             
            var datatable = $('<?php echo '#table_name'; ?>').dataTable( {
                
                        destroy: true,
                      

                        "order": [[ 1, "asc" ]],
                        "searching": false,
                        "paging": true,
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                        
                            "url": "<?php echo base_url(); ?>trace/<?php echo $url_json; ?>?array_index=<?php echo $array_index; ?>&row_date=<?php echo $row_date; ?>&row_error=<?php echo $row_error; ?>",
                            "type": "POST",
            
                        },
                        
                        <?php if ($array_index = 'celcom') { ?>

                         
                        "columnDefs": [     
                                                    
                                                    { "targets": 0, "data": 0},
                                                    { "targets": 1, "data": 1},
                                                    { "targets": 2, "data": 2}
    
                                                ],
                                                
                        "columnDefs": [ 
                        
                                                    {
                                                        "targets": 3,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return '  50';
                                                                        }
                                                    },
                                                    
                                                    {
                                                        "targets": 4,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return ' WAP ';
                                                                        }
                                                    },
                                                    
                                                     {
                                                        "targets": 5,
                                                        "data": 3,
                                                        "render": function ( data, type, full, meta ) {
                                                                        return '   <td><div class="datatable-ellipse" title="'+data+'">'+data+'</div></td>';
                                                                    }    
                                                    },
                                                    
                                                    {
                                                        "targets": 6,
                                                        "data": 4,
                                                        "render": function ( data, type, full, meta ) {
                                                                        return  data;
                                                                    }    
                                                    }
                                              
                                              ]
                        
                        <?php } else {?>

                        
                        "columnDefs": [ 
                        
                                                    {
                                                        "targets": 5,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return '   <td><div class="datatable-ellipse" title="'+data+'">'+data+'</div></td>';
                                                                        }
                                                    }
                                              
                                              ]
                        
                        <?php } ?>

            });

                
        } 
        
    })

</script>

        
        