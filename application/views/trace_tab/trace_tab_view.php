<?php 
$default_date_from = ($this->session->userdata($array_index.'_date_from')) ? $this->session->userdata($array_index.'_date_from') : date("d-m-Y", strtotime(' -1 day'));
$default_date_to = ($this->session->userdata($array_index.'_date_to')) ? $this->session->userdata($array_index.'_date_to') : date("d-m-Y"); 
?>
                
                <div class="row">
                    <div class="col-lg-12" id="<?php echo $table_name.'_scroll_to'; ?>">
                        <a href="<?php echo base_url().'trace/get_errors_by_carrier/'.$array_index?>" style="text-decoration:none;"><h1 class="page-header"><?php echo $title; ?><small id="<?php echo $table_name.'_sm1'; ?>"></small><small id="<?php echo $table_name.'_sm2'; ?>"></small></h1></a>
                        <div class="input-group col-lg-4">
                            <span class="input-group-addon" id="basic-addon1">Date</span>
                            <input type="text" class="form-control" name="<?php echo $daterange_name; ?>" value="<?php echo $default_date_from; ?> - <?php echo $default_date_to; ?>" aria-describedby="basic-addon1"/>
                        </div>
                        
                        <div class="form-group">
                            <br>
                            <table id="<?php echo $table_name; ?>" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Ceated at</th>
                                        <th>Carrier error code</th>
                                        <th>Carrier description</th>
                                        <th>Total</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                </tbody>   
                            </table>
                        </div>    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                <div class="row" >
                    <div class="col-lg-12"  id="<?php echo $table_name.'_errors_hidden'; ?>" style="display:none;">
                        <h2 class="text-info"><i class="fa fa-minus-circle "></i> <?php echo $title; ?> - Error info</h2>
                        
                        <div class="form-group" >
                            <br>
                            <table id="<?php echo $table_name.'_erros_detail'; ?>" class="table table-striped table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th>Created at</th>
                                        <th>Carrier response code</th>
                                        <th>Event</th>
                                        <th>Service</th>
                                        <th>Method</th>
                                        <th>Request</th>
                                        <th>Response</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                </tbody>   
                            </table>
                        </div>    
                        <div class="row" >
                            <div class="col-lg-12" >
                                <h3 class="text-info"><a class=" btn btn-success " id="<?php echo $table_name.'_button_hidde'; ?>" href="#"><i class="fa fa-level-up"></i><?php echo ' Hide '. $title . ' info'; ?></a> </h3>
          
                            </div>
                        <!-- /.col-lg-12 -->
                        </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

<script>

    var date_from;
    var date_to;

    $(document).ready(function() {
        
        window.onload = load_datatable();
        
        $('input[name="<?php echo $daterange_name; ?>"]').daterangepicker(
            {
                locale: {
                    format: 'DD-MM-YYYY'
                }
            }, 
            
            function(start, end, label) {
                
                var date_from = start.format('DD-MM-YYYY');
                var date_to = end.format('DD-MM-YYYY');
                load_datatable(date_from, date_to);
                
            }
            
        );
     
        function load_datatable(date_from, date_to) {
                        
            if(typeof date_from === 'undefined'){
                
                var date_from ="<?php echo $default_date_from; ?>";
                var date_to = "<?php echo $default_date_to; ?>";
                
            };

            $.ajax({
                
                "url": "<?php echo base_url(); ?>trace/get_filtered_totals?array_index=<?php echo $array_index; ?>&date_from="+date_from+"&date_to="+date_to+"", 
                "dataType": 'json', 
                "success": function(result){
               // TODO avoid id html identifiers 
                $("<?php echo '#'. $table_name.'_sm1'; ?> ").html(" <i>Transactions: "+result.total_not_filtered+". </i>");
                $("<?php echo '#'. $table_name.'_sm2'; ?>").html("<i>Errors: "+result.total_filtered+".</i>");
            
            }});
             
            var datatable = $('<?php echo '#'.$table_name; ?>').dataTable( {
                
                        destroy: true,
                        "order": [[ 0, "desc" ]],
                        "searching": false,
                        "paging": true,
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                            "url": "<?php echo base_url(); ?>trace/get_errors?array_index=<?php echo $array_index; ?>&date_from="+date_from+"&date_to="+date_to+"",
                            "type": "POST",
                        },
                        
                        "columns": [
                                { "data": 0, "targets": [0]}, 
                                { "data": 1, "targets": [1]}, 
                                { "data": 2, "targets": [2]},
                                { "data": 3, "targets": [3]}
                              
                        ],
                  
                        "columnDefs": [ 
                        
                            {
                                "data": 0,
                                "targets": 0,
                                "render": function ( data, type, full, meta ) {
                                    
                                                    var d = new moment(data);
                                                    var datestring = d.format('DD-MM-YYYY');

                                                    javascriptDate = "<div id="+data+">"+datestring+"</div>";
                                                                                                       
                                                    return javascriptDate; 
                                                    
                                                }    
                                               
                            },
                            
                            {
                                "data": null,
                                "defaultContent": "<p><strong style=\"padding:0px;\" class=\"alert alert-danger alert-dismissible\">Warning! Unknown error recieved.</strong></p>",
                                "targets": 2
                            },
                            
                            {
                                "targets": 4,
                                "data": 4,
                                "render": function ( data, type, full, meta ) {
                                                    return '<a class=\" btn btn-primary btn-sm \" style=\"width:100%;\" href=\"#\"><i class="fa fa-plus"></i></a>';
                                                }
                            }
                            
                        ], 
                        
            });
                
        } 
        
        $( "<?php echo '#'.$table_name; ?> tbody " ).on( "click", "a", function() {
            $("<?php echo '#'. $table_name.'_errors_hidden'; ?>").slideDown('slow');
        });
        
        $( "<?php echo '#'. $table_name.'_button_hidde'; ?>" ).click(function() {
            $("<?php echo '#'. $table_name.'_errors_hidden'; ?>").slideUp("slow");
            $('html,body').animate({scrollTop:  $('<?php echo '#'.$table_name.'_scroll_to'; ?> ').offset().top});
        });
        
        $( "<?php echo '#'.$table_name; ?> tbody " ).on( "click", "a", function() {
            
            var row_date = $(this).parent().parent("tr:first").children("td:first").children("div:first").attr("id");
            var row_error = $(this).parent().parent("tr:first").children("td").eq(1).text();
            $('html,body').animate({scrollTop:  $('<?php echo '#'. $table_name.'_errors_hidden'; ?>').offset().top});
           
            
            //$(this).attr("href", "<?php echo base_url(); ?>trace/get_row_error?array_index=<?php echo $array_index; ?>&row_date="+row_date+"&row_error="+row_error+"");

            var datatable = $('<?php echo '#'.$table_name.'_erros_detail'; ?>').dataTable( {
                
                        destroy: true,
                      

                        "order": [[ 1, "asc" ]],
                        "searching": false,
                        "paging": true,
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                        
                            "url": "<?php echo base_url(); ?>trace/get_row_errors_json?array_index=<?php echo $array_index; ?>&row_date="+row_date+"&row_error="+row_error+"",
                            "type": "POST",
            
                        },
                        
                        <?php if ($array_index = 'celcom') { ?>

                         
                        "columnDefs": [     
                                                    
                                                    { "targets": 0, "data": 0},
                                                    { "targets": 1, "data": 1},
                                                    { "targets": 2, "data": 2}
    
                                                ],
                                                
                        "columnDefs": [ 
                                                     
                                                    
                                                    {
                                                        "targets": 0,
                                                        "data": 0,
                                                        "render": function ( data, type, full, meta ) {
                                        
                                                                            var javascriptDate = new Date(data);
                                                                            var month = javascriptDate.getMonth();
                                                                            var date = javascriptDate.getDate();
                                                                            
                                                                             
                                                                            
                                                                            if ( month < 10) {
                                                                                   
                                                                                   month = "0"+javascriptDate.getMonth();
                                                                                   
                                                                            }
                                                                           
                                                                            if ( date < 10 ) {
                                                                               
                                                                               date  = "0" +javascriptDate.getDate(); 
                                                                               console.log(date);
                                                                            }
                                                                           
                                                                            javascriptDate = "<div id="+data+">"+date+"-"+month+"-"+javascriptDate.getFullYear()+"</div>";
                        
                                                                            return javascriptDate;
                                                                        }
                                                    },
                                                    
                                                    {
                                                        "targets": 3,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return '  50';
                                                                        }
                                                    },
                                                    
                                                    {
                                                        "targets": 4,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return ' WAP ';
                                                                        }
                                                    },
                                                    
                                                     {
                                                        "targets": 5,
                                                        "data": 3,
                                                        "render": function ( data, type, full, meta ) {
                                                                        return '   <td><div class="datatable-ellipse" title="'+data+'">'+data+'</div></td>';
                                                                    }    
                                                    },
                                                    
                                                    {
                                                        "targets": 6,
                                                        "data": 4,
                                                        "render": function ( data, type, full, meta ) {
                                                                        return  data;
                                                                    }    
                                                    }
                                              
                                              ]
                        
                        <?php } else {?>

                        
                        "columnDefs": [ 
                        
                                                    {
                                                        "targets": 5,
                                                        "data": 5,
                                                        "render": function ( data, type, full, meta ) {
                                                                            return '   <td><div class="datatable-ellipse" title="'+data+'">'+data+'</div></td>';
                                                                        }
                                                    }
                                              
                                              ]
                        
                        <?php } ?>

            });
            
            
            
        });
        
        
        
    })

</script>

        